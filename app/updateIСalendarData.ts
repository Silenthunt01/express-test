import ical from 'node-ical'
import axios from 'axios';
import { icalDifference } from '../helpers/icalDifference';
import fs from 'fs';
import config from '../config'

export const readData = (): ical.CalendarResponse => {
  return JSON.parse(fs.readFileSync('data.json', 'utf8')); //sync get data from data.json file
}

const writeData = (data: ical.CalendarResponse) => {
  return fs.writeFileSync('data.json', JSON.stringify((data))) //write updated data to file
}

const update = async() => {
  const {fullEntries} = await updateICalendarData()
  writeData(fullEntries)
}

export const installICalendarUpdateInterval = () => {
  console.log('config: ', config)
  update() //first update
  setInterval(async() => {
    update() //regular update 
  }, config.ical.updateTimeout) //get timeout from config
}


const updateICalendarData = async () => {
  try {
    const {data} = await axios.get(config.ical.url) //get data from calendar service
    const parsed = ical.parseICS(data) //parse it, using node-ical lib
    let newCalendarEntries: ical.CalendarResponse = {} //new object, where we are going to store updated entries
    const storedCal = readData() //read existing data from file
    const newEntries = icalDifference(storedCal, parsed) //get difference between our object and outer one
    //result is in string array
    newEntries.forEach((item) => {
      newCalendarEntries[item] = parsed[item] //for each string in difference array get real calendar object and insert it in our storage object
    })
    console.log(newEntries) //log difference
    return {newEntries: newCalendarEntries, fullEntries: { ...parsed, ...newCalendarEntries}} //return new entries
    //and all entries
  } catch(e) {
    console.log(e)
    return {newEntries: {}, fullEntries: {}}
  }
}