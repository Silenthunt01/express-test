import express, { Application, Request, Response } from "express";
import { installICalendarUpdateInterval, readData } from "./app/updateIСalendarData";

const app: Application = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get(
    "/",
    async (req: Request, res: Response): Promise<Response> => {
        return res.status(200).send({
            name: "ICalendar",
        });
    }
);
app.get(
    "/getData",
    async (req: Request, res: Response): Promise<Response> => {
        const data = readData() //read data from json file
        return res.status(200).send(data);
    }
);

try {
    app.listen(port, (): void => {
        console.log(`Connected successfully on port ${port}`)
    });
} catch (error) {
    console.error(`Error occured: ${(error as any).message}`)
}

installICalendarUpdateInterval() //install update interval