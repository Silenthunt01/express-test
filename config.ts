require('dotenv').config(); //read .env file

const dev = {
 ical: {
   updateTimeout: parseInt(process.env.ICAL_UPDATE_TIMEOUT || '15000'), //default is 15 sec
   url: process.env.ICAL_URL || '',
 }
};

const config = {
 dev,
};

export default config.dev;