import ical from 'node-ical'

export const icalDifference = (original: ical.CalendarResponse, updated: ical.CalendarResponse) => {
  const originalKeys = Object.keys(original) //get keys from original calendar object
  const updatedKeys = Object.keys(updated) //get keys from updated calendar object
  return updatedKeys.reduce((acc, rec) => {
    if (!originalKeys.includes(rec)) { //if no entry found in old array add string with key to reduce acc
      return [...acc, rec]
    } 
    return acc
  }, [] as string[])// return string array of keys presented in updated calendar, but not found in original 
}